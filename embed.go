package helical

import (
	"fmt"
)

func Embed(conf *Config, vod string) string {
	return fmt.Sprintf(EMBED_TMPL, vod, conf.embedParent)
}

const EMBED_TMPL = `<iframe
  src="https://player.twitch.tv/?%s&muted=true&autoplay=false&parent=%s"
  height="300"
  width="400"
  allowfullscreen="true"></iframe>`
