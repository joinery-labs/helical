package helical

import (
	"fmt"
	"os"
)

// configuration
// to make environment variables available to testing
const EXTENSION_ID = "EXTENSION_ID"

type Config struct {
	extensionId string
	apiPartner  string
	apiToken    string
	embedParent string
}

func NewConfig() *Config {
	return &Config{}
}

func (c *Config) ExtensionId(id string) {
	c.extensionId = id
}

// Twitch application client ID
func (c *Config) APIPartner(id string) {
	c.apiPartner = id
}

// Twitch application credentials token
func (c *Config) APIToken(token string) {
	c.apiToken = token
}

// Twitch embedded player parent-window site
func (c *Config) EmbedParent(parent string) {
	c.embedParent = parent
}

func (c *Config) Hostname() string {
	// format the hostname for the CORS allow-origin
	// 1. For Netlify, EXTENSION_ID environment variable should be defined
	// 2. Locally for testing, rely on configuration field
	if c.extensionId != "" {
		return fmt.Sprintf("https://%s.ext-twitch.tv", c.extensionId)
	}

	cid := os.Getenv(EXTENSION_ID)
	if cid != "" {
		c.extensionId = cid
		return fmt.Sprintf("https://%s.ext-twitch.tv", c.extensionId)
	}

	return ""
}
