package helical

import (
	"fmt"
	"log"

	"github.com/pkg/errors"
)

func Users(conf *Config, username string) []byte {
	end := fmt.Sprintf(TMPL_USERS, username)
	conn := &connector{
		Token:   conf.apiToken,
		Partner: conf.apiPartner,
	}

	body, err := conn.get(end)
	if err != nil {
		log.Fatal(errors.Wrap(err, "ERROR get users"))
	}

	return body
}
